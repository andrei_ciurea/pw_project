--ROLE
CREATE TABLE IF NOT EXISTS roles (
	id		SERIAL		PRIMARY KEY,
	role_name	TEXT		NOT NULL
);

--USERS
CREATE TABLE IF NOT EXISTS users (
	id		SERIAL 		PRIMARY KEY,
	last_name	TEXT,
	first_name	TEXT,
	email		TEXT 		NOT NULL UNIQUE,
	password	TEXT		NOT NULL,
	id_role		integer		REFERENCES roles (id)
);

CREATE TABLE IF NOT EXISTS privileges (
	id		SERIAL		PRIMARY KEY,
	prv_name	TEXT		NOT NULL
);

--ROLE_PRIVILEGE
CREATE TABLE IF NOT EXISTS role_privilege (
	id		SERIAL		PRIMARY	KEY,
	id_role		integer		REFERENCES roles (id),
	id_prv		integer		REFERENCES privileges (id)
);

--SUBJECT TABLE
CREATE TABLE IF NOT EXISTS subjects (
	id		SERIAL		PRIMARY KEY,	
	subject_name TEXT,
	subject_desc TEXT
);

CREATE TABLE IF NOT EXISTS subject_user(
	id		SERIAL		PRIMARY KEY,
	id_subject	integer		REFERENCES subjects (id),
	subject_student_grade float
	id_user		integer		REFERENCES users (id),
);

CREATE TABLE IF NOT EXISTS tasks (
	id		SERIAL		PRIMARY KEY,
	task_name	TEXT,
	task_desc	TEXT,
	task_start	timestamp,
	task_end	timestamp,
	task_submision	TEXT,
	id_subject	integer		REFERENCES subjects(id)
);

CREATE TABLE IF NOT EXISTS submission (
	id		SERIAL		PRIMARY KEY,
	submission_path	TEXT,
	id_task		integer		REFERENCES tasks(id),
	id_user		integer,
	submission_date	timestamp
);

CREATE TABLE IF NOT EXISTS comm (
	id		SERIAL		PRIMARY KEY,
	comm_text	TEXT		NOT NULL,
	comm_createTime	timestamp,
	id_task		integer		REFERENCES tasks(id),
	id_user		integer		REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS likes (
	id		SERIAL		PRIMARY KEY,
	id_comm		integer		REFERENCES comm(id),
	id_user		integer		REFERENCES users(id),
	l_d		integer
);

CREATE TABLE IF NOT EXISTS heatmap(
	id		SERIAL		PRIMARY KEY,
	clientX		integer,
	clientY		integer,
	page		TEXT,
	counter		integer
);
