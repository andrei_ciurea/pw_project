
$(document).ready(function(){
    var heatmap_page1 = document.querySelector('#heatmap_page1');
    var heatmap_page2 = document.querySelector('#heatmap_page2');

    heatmap_page1.addEventListener('click', function(){heatm('page1') });
    heatmap_page2.addEventListener('click', function(){heatm('page2') });

    function heatm(page){
        var sendInfo = {
            page_name : page
        };
        console.log(sendInfo);
        $.ajax({
            url: "../../api2/welcome/welcome_heatmap_select.php",
            type: "POST",
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
            dataType: "json",
            data: JSON.stringify(sendInfo),
            error: function (err) {
            console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
            },
            success: function(data) {
                //alert(JSON.stringify(data)); // apple
                var hm;
                hm = h337.create({
                element: document.body,
                radius: 20,
                visible: true,
                opacity: 30
                });
                //hm.store.generateRandomDataSet(400);
                hm.store.insertData(data["max"],data["records"])
                hm.get('canvas').style.zIndex = 1;

                var canvas = document.getElementsByTagName('canvas');
                console.log(canvas);
                canvas[0].addEventListener("click",function(){
                    console.log("cleanup1");
                    hm.cleanup();
                },false);
            }
        });
    };
});