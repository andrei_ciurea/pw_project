//var dict_to_redirect = {};

var counterPage1 = 0;
var myIntervalPage1 = null;

var counterPage2 = 0;
var myIntervalPage2 = null;

var arrayList = [];
var heatmapObj = {};

$(document).ready(function(){

  //mouseOver page1

  $(".page1").hover(function(e){
    counterPage1 = 0;
    myIntervalPage1 = setInterval(function () {
        ++counterPage1;
    }, 50);
    
    },function(e){
        clearInterval(myIntervalPage1);
        //alert(counterPage1);
        heatmapObj["clientX"] = e.pageX;
        heatmapObj["clientY"] = e.pageY;
        heatmapObj["page"] = "page1";
        heatmapObj["counter"] = counterPage1;
        arrayList.push(heatmapObj);
        console.log(arrayList);
        heatmapObj = {};
    });


    $(".page2").hover(function(e){
      counterPage2 = 0;
      myIntervalPage2 = setInterval(function () {
          ++counterPage2;
      }, 50);
     // alert(e.clientX);
      },function(e){
          clearInterval(myIntervalPage2);
          heatmapObj["clientX"] = e.clientX;
          heatmapObj["clientY"] = e.clientY;
          heatmapObj["page"] = "page2";
          heatmapObj["counter"] = counterPage2;
          arrayList.push(heatmapObj);
          heatmapObj = {};
    });

  var ajax_call_heatMap = function() {
    if(arrayList.length > 0){
      $.ajax({
        url: "../../api2/welcome/welcome_heatmap.php",
        type: "POST",
        headers: {
            'Access-Control-Allow-Origin': '*',
          },
        dataType: "json",
        data: JSON.stringify(arrayList),
        error: function (err) {
          console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
        },
        success: function(data) {
            alert(JSON.stringify(data)); // apple
            arrayList = [];
        }
      });
    }
  };
  
  var interval = 1000 * 60 * 1; 
    
  setInterval(ajax_call_heatMap, interval);



  var tableTS = document.querySelector("#taskSubject");

  $.ajax({
    url: "../../api2/welcome/welcome_taskSubject.php",
    type: "GET",
    headers: {
        'Access-Control-Allow-Origin': '*',
      },
    dataType: "json",
    error: function (err) {
      console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
    },
    success: function(data) {
      //alert(JSON.stringify(data)); // apple

      var str = "<thead><tr>";
      var strbd = "<tbody>";
      $.each(data,function(key,value){
        var row = tableTS.insertRow(key);    
        var ind = 0;
        
        $.each(value,function(key,value){
          
          // cell = row.insertCell(ind);
          // cell.innerHTML = value;
          
          if(key != "id")
            str += ( "<th>" + key + "</th>");

            ind++;
        });
        
        str += "</tr></thead>";
        strbd += "<tr>";
        
        $.each(value,function(key,value){
          
          if(key != "id"){
            if(value == null)
              str += ( "<td>" + "" + "</td>");
            else
              str += ( "<td>" + value + "</td>");
          }
          
        });

        strbd += "</tr>";
      });
      str += "</tbody>"
      tableTS.insertAdjacentHTML('afterbegin',str);
      tableTS.insertAdjacentHTML('beforeend',strbd);
      // apple


      function handleRowClick(dict){
        return function(){
          //console.log(dict);
          
          console.log( "dict something = " + dict);
          var url = "http://andrei-x550jx:81/frontend/welcome/upload.html?";
          var count = 0;
          for(var key in dict){
            if(dict[key] != ""){
              count ++ ;
              url += key;
              url += "=";
              url += dict[key];
              if(count > 0){
                url += "&";
              }
            }
          }

          var page1 = document.querySelectorAll(".page1");
          for(var i = 0; i < page1.length; i++){
            page1[i].style.display = "none";
          }

          var page2 = document.querySelectorAll(".page2");
          for(var i = 0; i < page2.length; i++){
            page2[i].style.display = "";
          }

          console.log(dict);
          whenPage2Opens(dict);
          //location.replace(url);
        }
      }
    
      var table = document.getElementById("taskSubject");
      var rows = table.getElementsByTagName('tr');
      //alert("rows = " + rows.length); 
      for(var i = 1; i < rows.length; i++){
        var rw = rows[i].innerText;
        var dict = [];
        var header = rows[i].parentElement.parentElement.getElementsByTagName('thead')[0].innerText.split("\n")[0].split("\t");
        var values = rw.split("\t");
        header.forEach((key,i) => {
          if(i < 2)
            dict[key] = values[i];
        });
        //dict_to_redirect = dict;
        //console.log(dict);
        rows[i].addEventListener('click',handleRowClick(dict), false);
      }

      //creates a sitemap
      
      siteMapper();
     }
  });

  var tableSub = document.querySelector("#subject");

  $.ajax({
    url: "../../api2/welcome/welcome_Subject.php",
    type: "GET",
    headers: {
        'Access-Control-Allow-Origin': '*',
      },
    dataType: "json",
    //async: false,
    error: function (err) {
      console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
    },
    success: function(data) {
      //alert(JSON.stringify(data)); // apple

      var str = "<thead><tr>";
      var strbd = "<tbody>";
      $.each(data,function(key,value){
        var row = tableSub.insertRow(key);    
        var ind = 0;

        $.each(value,function(key,value){
          
          // cell = row.insertCell(ind);
          // cell.innerHTML = value;
          // dict[key] = value;
          if(key != "id")
            str += ( "<th>" + key + "</th>");
          
            ind++;
        });
        
        str += "</tr></thead>";
        strbd += "<tr>";
        
        $.each(value,function(key,value){
          
          if(key != "id"){
            if(value == null)
              str += ( "<td>" + "" + "</td>");
            else
              str += ( "<td>" + value + "</td>");
          }
          
        });

        strbd += "</tr>";
      });
      str += "</tbody>"
      tableSub.insertAdjacentHTML('afterbegin',str);
      tableSub.insertAdjacentHTML('beforeend',strbd);
      // apple

      function handleRowClick(dict){
        return function(){
          console.log(dict);
          var url = "http://andrei-x550jx:81/frontend/welcome/upload.html?";
          var count = 0;
          for(var key in dict){
            if(dict[key] != ""){
              count ++ ;
              url += key;
              url += "=";
              url += dict[key];
              if(count > 0){
                url += "&";
              }
            }
          }
          //location.replace(url);
        }
      }
    
      var table = document.getElementById("subject");
      var rows = table.getElementsByTagName('tr');
      for(var i = 1; i < rows.length; i++){

        var rw = rows[i].innerText;
        var dict = [];
        var header = rows[i].parentElement.parentElement.getElementsByTagName('thead')[0].innerText.split("\n")[0].split("\t");
        var values = rw.split("\t");
        header.forEach((key,i) => {
          if(i < 1)
            dict[key] = values[i];
        });
        
        rows[i].addEventListener('click',handleRowClick(dict), false);
      }

     }
     
  });
  
  //table the most important
  
  var tableMst = document.querySelector("#mostImpoortant");

  $.ajax({
    url: "../../api2/welcome/welcome_MST.php",
    type: "GET",
    headers: {
        'Access-Control-Allow-Origin': '*',
      },
    dataType: "json",
    //async: false,
    error: function (err) {
      console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
    },
    success: function(data) {
     // alert(JSON.stringify(data)); // apple
      if(data.length > 0){
        var str = "<thead><tr>";
        var strbd = "<tbody>";
        $.each(data,function(key,value){
          var row = tableMst.insertRow(key);    
          var ind = 0;

          $.each(value,function(key,value){
            
            // cell = row.insertCell(ind);
            // cell.innerHTML = value;
            // dict[key] = value;
            if(key != "id")
              str += ( "<th>" + key + "</th>");
            
              ind++;
          });
          
          str += "</tr></thead>";
          strbd += "<tr>";
          
          $.each(value,function(key,value){
            
            if(key != "id"){
              if(value == null)
                str += ( "<td>" + "" + "</td>");
              else
                str += ( "<td>" + value + "</td>");
            }
            
          });

          strbd += "</tr>";
        });
        str += "</tbody>"
        tableMst.insertAdjacentHTML('afterbegin',str);
        tableMst.insertAdjacentHTML('beforeend',strbd);
      } 
    }
  });
  
});



// function handleRowClick(e){
//   e.preventDefault;
//   console.log(e);
// }

// //rows of a table -- click event :>
// var rows = document.getElementsByTagName('tr');
// alert("rows = " + rows.length);
// for(var row in rows){
//   //console.log("som");
//   console.log(row);
//   row.addEventListener('click', handleRowClick);
// }

var form = document.querySelector(".form-login");

form.addEventListener("submit", postFunction);

function postFunction(e) {
    e.preventDefault();
    
    request = $.ajax({
        url: "../../api2/login/getsessionvar.php",
        type: "GET",
        headers: {
            'Access-Control-Allow-Origin': '*',
          },
        dataType: "json",
        error: function (err) {
          console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
        },
        success: function(data) {
            //alert(JSON.stringify(data)); // apple
         }
      });
}



// const navSlide = () => {
//   const burger = document.querySelector('.burger');
//   const nav = document.querySelector('.nav-links');
//   const navLinks = document.querySelectorAll('.nav-links li');
//   //toggle nav

//   burger.addEventListener('click',()=>{
//       nav.classList.toggle('nav-active');
//       //console.log(navLinks);
//       //animate links
//       navLinks.forEach((link,index) => {
        
//         console.log(link.style.animationDirectionDirection == 'normal');
        
//         if(link.style.animation){
//           link.style.animation = '';
//         }
//         else{
//           link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7 + 0.3}s`;
//         }
//       });
//       //burger animation
//       burger.classList.toggle('toggle');
//   });
  
  
// }

// navSlide();

// upload.js

function whenPage2Opens (dict_to_redirect){

  var sendInfo = {};
  
  // var queryString = decodeURIComponent(window.location.search);
  // queryString = queryString.substring(1);
  // var queries = queryString.split("&");
  // for (var i = 0; i < queries.length; i++)
  // {
  //     let key = queries[i].split('=')[0];
  //     let value = queries[i].split('=')[1];
  //     if(typeof value !== 'undefined'){
  //         sendInfo[key] = value;
  //     }
  // }
  console.log(dict_to_redirect["subject_name"]);
  for(var key in dict_to_redirect){
    if(typeof dict_to_redirect[key] !== 'undefined'){
      sendInfo[key] = dict_to_redirect[key];
      //console.log(dict_to_redirect[key]);
    }
  }

  //console.log(JSON.stringify(sendInfo));

  $.ajax({
      url: "../../api2/welcome/welcome_comm_like.php",
      type: "POST",
      headers: {
          'Access-Control-Allow-Origin': '*',
      },
      data: JSON.stringify(sendInfo),
      dataType: "json",
      error: function (err) {
        console.log("AJAX error in request: " + JSON.stringify(err, null, 2));

      },
      success: function(data) {
          alert(JSON.stringify(data)); // apple
          //TODO: add the form of comms
          var divColumns = $('#comms');
          for(var i = 0; i < data.length; i++){

              var comDiv = document.createElement('div');
              var label = document.createElement('label');
              var button = document.createElement('button');
              var buttonDislike = document.createElement('button');
              var labelTime = document.createElement('label');
              var divAC = document.createElement('div');

              label.innerHTML += data[i]["email"];
              button.innerHTML += "Like"  +": " + data[i]["nr_likes"];
              buttonDislike.innerHTML += "Dislike: " + data[i]["nr_dislikes"];
              labelTime.innerHTML += data[i]["comm_createTime"];
              divAC.innerHTML += data[i]["comm_text"];

              button.addEventListener('click', function(){
                
//                  console.log(this.parentNode.querySelector('label').innerHTML);
                  this.innerHTML = "Like: " + (parseInt(this.innerHTML.split("Like: ")[1]) + 1);
                  var email = this.parentNode.querySelector('label').innerHTML;
                  var crp = this.parentNode.querySelector('div').innerHTML;
                  //console.log(JSON.stringify({comm_text: crp}));
                  $.ajax({
                    url: "../../api2/welcome/welcome_select_idcm.php",
                    type: "POST",
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                    },
                    data: JSON.stringify({comm_text: crp}),
                    dataType: "json",
                    error: function (err) {
                        console.log("AJAX error in request1: " + JSON.stringify(err, null, 2));
                    },
                    success: function(data) {
                      alert(JSON.stringify(data));
                      var sdn = {"email": email, "id_comm": data[0]["id"], "l_d":1};
                      //console.log("ceva" + JSON.stringify(sdn));
                      $.ajax({
                          url: "../../api2/welcome/welcome_uploadLike.php",
                          type: "POST",
                          headers: {
                              'Access-Control-Allow-Origin': '*',
                          },
                          data: JSON.stringify(sdn),
                          dataType: "json",
                          error: function (err) {
                              console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
                          },
                          success: function(data) {
                              alert(JSON.stringify(data));
                          }
                      });
                  }
                });
              });


              buttonDislike.addEventListener('click', function(){
                this.innerHTML = "Dislike: " + (parseInt(this.innerHTML.split("Dislike: ")[1]) + 1);

                var email = this.parentNode.querySelector('label').innerHTML;
                var crp = this.parentNode.querySelector('div').innerHTML;
                console.log(crp);
                $.ajax({
                  url: "../../api2/welcome/welcome_select_idcm.php",
                  type: "POST",
                  headers: {
                      'Access-Control-Allow-Origin': '*',
                  },
                  data: JSON.stringify({comm_text: crp}),
                  dataType: "json",
                  error: function (err) {
                      console.log("AJAX error in request1: " + JSON.stringify(err, null, 2));
                  },
                  success: function(data) {
                      //alert(JSON.stringify(data));
                    var sdn = {"email": email, "id_comm": data[0]["id"], "l_d":1};
                    console.log(sdn);
                    $.ajax({
                        url: "../../api2/welcome/welcome_uploadLike.php",
                        type: "POST",
                        headers: {
                            'Access-Control-Allow-Origin': '*',
                        },
                        data: JSON.stringify(sdn),
                        dataType: "json",
                        error: function (err) {
                            console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
                        },
                        success: function(data) {
                            //alert(JSON.stringify(data));
                        }
                    });
                }
              });
              });
              
              comDiv.append(label);
              comDiv.append(button);
              comDiv.append(buttonDislike);
              comDiv.append(labelTime);
              comDiv.append(divAC);
              divColumns.append(comDiv);
          }
          
      }
      
  });




  const url = '../../api2/upload/upload.php';
  const formUpload = document.querySelector('.form-upload');



  formUpload.addEventListener('submit', e => {
    e.preventDefault()

    const files = document.querySelector('[type=file]').files;
    const formData = new FormData();
    for (let i = 0; i < files.length; i++) {
        let file = files[i];
      
        formData.append('files[]', file);
    }

    // var queryString = decodeURIComponent(window.location.search);

    // queryString = queryString.substring(1);
    // var queries = queryString.split("&");
    // for (var i = 0; i < queries.length; i++)
    // {
    //     let key = queries[i].split('=')[0];
    //     let value = queries[i].split('=')[1];
    //     if(typeof value !== 'undefined'){
    //        formData.append(key, value);
    //        // console.log(value);
    //     }
    // }

    for(var key in dict_to_redirect){
      if(typeof dict_to_redirect[key] !== 'undefined'){
        formData.append(key, dict_to_redirect[key]);
      // console.log(value);
      }
    }
    //formData.append("subject", "pw");
    

    $.ajax({
        url: url,
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        beforeSend: function() {
            var progress = document.querySelector('progress');
            progress.value = 0;
        },
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = (evt.loaded / evt.total) * 100;
                    //Do something with upload progress here
                    
                    var progress = document.querySelector('progress');
                    progress.value = percentComplete;
                }
          }, false);
          return xhr;
        },
        error: function (err) {
            console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
        },
        success: function(data){
          //alert(JSON.stringify(data));
          var progress = document.querySelector('progress');
          progress.value = 0;
        }
      });
  });



var frm = document.querySelector(".form-commSub");

frm.addEventListener("submit", postFunction);

function postFunction(e) {
  e.preventDefault();
 
  // console.log("post function");
  console.log(document.getElementById("comm_text").value);
  console.log(document.getElementById("comm_text").value);

  var sendInfos = {
      comm_text: document.getElementById("comm_text").value,
  };

  for(var key in sendInfo){
    if(typeof sendInfo[key] !== 'undefined'){
      sendInfos[key] = sendInfo[key];
    
    }
  }

   console.log(JSON.stringify(sendInfos));
   console.log(JSON.stringify(sendInfo));
   
  request = $.ajax({
      url: "../../api2/welcome/welcome_insertComm.php",
      type: "POST",
      headers: {
          'Access-Control-Allow-Origin': '*',
        },
      dataType: "json",
      data: JSON.stringify(sendInfos),
      error: function (err) {
          console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
      },
      success: function(data) {
          //alert(JSON.stringify(data)); // apple
          console.log(data);
      }
  });
     
}
};

function siteMapper(){
  console.log($("#taskSubject"));
  $.ajax({
    url: "../../api2/welcome/welcome_crawler.php",
    type: "GET",
    headers: {
        'Access-Control-Allow-Origin': '*',
      },
    dataType: "json",
    //data: JSON.stringify(sendInfo),
    error: function (err) {
        console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
    },
    success: function(data) {
        alert(JSON.stringify(data)); // apple
        
        appendDom($('.footer'),data);
    }
  });
  function appendDom(container, jsonData) {
    
    for (var i = 0; i <jsonData.length; i++) {
        var $divParent  = $("<div></div>");
        $divParent.text(jsonData[i].url).attr('id',jsonData[i].id).attr('style',"margin-left: 2em");
        console.log(jsonData[i].id);
        if (jsonData[i].children) {
            appendDom($divParent, jsonData[i].children);
        }
        container.append($divParent);
    }
  }

}

