var logout = document.querySelector("#logout");
logout.addEventListener("click", logoutFunction);

function logoutFunction(e){
  e.preventDefault();

  request = $.ajax({
    url: "../../api2/login/logout.php",
    type: "POST",
    headers: {
        'Access-Control-Allow-Origin': '*',
      },
    dataType: "json",
    error: function (err) {
      console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
    },
    success: function(data) {
        alert(JSON.stringify(data)); // apple
        location.replace(data["location"]);
     }
  });
}



const navSlide = () => {
    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.nav-links');
    const navLinks = document.querySelectorAll('.nav-links li');
    //toggle nav
  
    burger.addEventListener('click',()=>{
        nav.classList.toggle('nav-active');
        //console.log(navLinks);
        //animate links
        navLinks.forEach((link,index) => {
          
          console.log(link.style.animationDirectionDirection == 'normal');
          
          if(link.style.animation){
            link.style.animation = '';
          }
          else{
            link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7 + 0.3}s`;
          }
        });
        //burger animation
        burger.classList.toggle('toggle');
    });
    
    
  }
  
  navSlide();

$(document).ready(function(){ 
    var searcher = document.querySelector("#searcher");
    searcher.addEventListener('input',function(){
        //console.log(this.value);

        var filter = this.value;
        function walkTheDOM(node, func)
        {
            //console.log(node.parentNode);
            func(node);
            node = node.firstChild;
            while (node)
            {
                walkTheDOM(node, func);
                node = node.nextSibling;
            }
        }

        function removeAll(node)
        {
            // protect against "node === undefined"
             //console.log(node.textContent + " " + node.);
            //if (node && node.nodeType === 3) // TEXT_NODE
            {
                if(filter == ''){
                    node.style = "background-color: none";
                }else{
                    if (node.textContent.indexOf(filter) !== -1) // contains offending text
                    {
                        node.style = "background-color: yellow;";
                        node.parentNode.parentNode.style = "background-color: none";
                    }else{
                        node.style = "background-color: none";
                    }
                }
            }
        }

        walkTheDOM(document.getElementsByTagName("BODY")[0], removeAll);
    });

    $.ajax({
      url: "../../api2/welcome/welcome_admin.php",
      type: "GET",
      headers: {
          'Access-Control-Allow-Origin': '*',
        },
      dataType: "json",
      error: function (err) {
        console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
      },
      success: function(data) {
          alert(JSON.stringify(data)); // apple
          if(data["role_name"] == "admin"){
            var ul = document.querySelector('.nav-links');
            var str = "  <li> <a href='http://andrei-x550jx:81/frontend/admin/index.html'>Admin</a> </li>";
            ul.insertAdjacentHTML('beforeend',str);

            var cl3 = document.querySelector('.column3');
            var page1_cl3 = cl3.querySelector('.page1');
            var str = "<button id='heatmap_page1'>Heatmap</button>";
            page1_cl3.insertAdjacentHTML('beforeend',str);

            var page2_cl3 = cl3.querySelector('.page2');
            var str = "<button id='heatmap_page2'>Heatmap</button>";
            page2_cl3.insertAdjacentHTML('beforeend',str);

            var heatmap_page1 = document.querySelector('#heatmap_page1');
            var heatmap_page2 = document.querySelector('#heatmap_page2');

            heatmap_page1.addEventListener('click', function(){heatm('page1') });
            heatmap_page2.addEventListener('click', function(){heatm('page2') });

            function heatm(page){
                var sendInfo = {
                    page_name : page
                };
                console.log(sendInfo);
                $.ajax({
                    url: "../../api2/welcome/welcome_heatmap_select.php",
                    type: "POST",
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                    },
                    dataType: "json",
                    data: JSON.stringify(sendInfo),
                    error: function (err) {
                    console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
                    },
                    success: function(data) {
                        alert(JSON.stringify(data)); // apple
                        var hm;
                        hm = h337.create({
                        element: document.body,
                        radius: 20,
                        visible: true,
                        opacity: 30
                        });
                        //hm.store.generateRandomDataSet(400);
                        hm.store.insertData(data["max"],data["records"])
                        hm.get('canvas').style.zIndex = 1;

                        var canvas = document.getElementsByTagName('canvas');
                        console.log(canvas);
                        canvas[0].addEventListener("click",function(){
                            console.log("cleanup1");
                            hm.cleanup();
                        },false);
                    }
                });
            };
          }
       }
    });

    //creates a sitemap
    //siteMapper();
});


// function siteMapper(){

//   $.ajax({
//     url: "../../api2/welcome/welcome_crawler.php",
//     type: "get",
//     headers: {
//         'Access-Control-Allow-Origin': '*',
//       },
//     dataType: "json",
//     //data: JSON.stringify(sendInfo),
//     error: function (err) {
//         console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
//     },
//     success: function(data) {
//         alert(JSON.stringify(data)); // apple
//         var footer = document.getElementsByTagName('footer');
//         footer.innerHTML = data;
//     }
//   });

// }