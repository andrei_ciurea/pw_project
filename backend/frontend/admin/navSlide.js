var logout = document.querySelector("#logout");
logout.addEventListener("click", logoutFunction);

function logoutFunction(e){
  e.preventDefault();

  request = $.ajax({
    url: "../../api2/login/logout.php",
    type: "POST",
    headers: {
        'Access-Control-Allow-Origin': '*',
      },
    dataType: "json",
    error: function (err) {
      console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
    },
    success: function(data) {
        alert(JSON.stringify(data)); // apple
        location.replace(data["location"]);
     }
  });
}



const navSlide = () => {
    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.nav-links');
    const navLinks = document.querySelectorAll('.nav-links li');
    //toggle nav
  
    burger.addEventListener('click',()=>{
        nav.classList.toggle('nav-active');
        //console.log(navLinks);
        //animate links
        navLinks.forEach((link,index) => {
          
          console.log(link.style.animationDirectionDirection == 'normal');
          
          if(link.style.animation){
            link.style.animation = '';
          }
          else{
            link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7 + 0.3}s`;
          }
        });
        //burger animation
        burger.classList.toggle('toggle');
    });
    
    
  }
  
  navSlide();

$(document).ready(function(){ 
    var searcher = document.querySelector("#searcher");
    searcher.addEventListener('input',function(){
        //console.log(this.value);

        var filter = this.value;
        function walkTheDOM(node, func)
        {
            //console.log(node.parentNode);
            func(node);
            node = node.firstChild;
            while (node)
            {
                walkTheDOM(node, func);
                node = node.nextSibling;
            }
        }

        function removeAll(node)
        {
            // protect against "node === undefined"
             //console.log(node.textContent + " " + node.);
            //if (node && node.nodeType === 3) // TEXT_NODE
            {
                if(filter == ''){
                    node.style = "background-color: none";
                }else{
                    if (node.textContent.indexOf(filter) !== -1) // contains offending text
                    {
                        node.style = "background-color: yellow;";
                        node.parentNode.parentNode.style = "background-color: none";
                    }else{
                        node.style = "background-color: none";
                    }
                }
            }
        }

        walkTheDOM(document.getElementsByTagName("BODY")[0], removeAll);
    });

    $.ajax({
      url: "../../api2/welcome/welcome_admin.php",
      type: "GET",
      headers: {
          'Access-Control-Allow-Origin': '*',
        },
      dataType: "json",
      error: function (err) {
        console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
      },
      success: function(data) {
          alert(JSON.stringify(data)); // apple
          if(data["role_name"] == "admin"){
            var ul = document.querySelector('.nav-links');
            var str = "  <li> <a href='http://andrei-x550jx:81/frontend/admin/index.html'>Admin</a> </li>";
            ul.insertAdjacentHTML('beforeend',str);
          }
       }
    });

});
