$(document).ready(function(){  
  var tableMst = document.querySelector("#mostImpoortant");

  $.ajax({
    url: "../../api2/welcome/welcome_MST.php",
    type: "GET",
    headers: {
        'Access-Control-Allow-Origin': '*',
      },
    dataType: "json",
    //async: false,
    error: function (err) {
      console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
    },
    success: function(data) {
      //alert(JSON.stringify(data)); // apple
      if(data.length > 0){
        var str = "<thead><tr>";
        var strbd = "<tbody>";
        $.each(data,function(key,value){
          var row = tableMst.insertRow(key);    
          var ind = 0;

          $.each(value,function(key,value){
            
            // cell = row.insertCell(ind);
            // cell.innerHTML = value;
            // dict[key] = value;
            if(key != "id")
              str += ( "<th>" + key + "</th>");
            
              ind++;
          });
          
          str += "</tr></thead>";
          strbd += "<tr>";
          
          $.each(value,function(key,value){
            
            if(key != "id"){
              if(value == null)
                str += ( "<td>" + "" + "</td>");
              else
                str += ( "<td>" + value + "</td>");
            }
            
          });

          strbd += "</tr>";
        });
        str += "</tbody>"
        tableMst.insertAdjacentHTML('afterbegin',str);
        tableMst.insertAdjacentHTML('beforeend',strbd);
      } 
      
    }
  });

  siteMapper();

//   document.querySelector( 'html' ).style.position = 'relative';
//   hm.style.position = 'absolute';

function siteMapper(){
  $.ajax({
    url: "../../api2/welcome/welcome_crawler.php",
    type: "GET",
    headers: {
        'Access-Control-Allow-Origin': '*',
      },
    dataType: "json",
    //data: JSON.stringify(sendInfo),
    error: function (err) {
        console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
    },
    success: function(data) {
        alert(JSON.stringify(data)); // apple
        
        appendDom($('.footer'),data);
    }
  });
  function appendDom(container, jsonData) {
    
    for (var i = 0; i <jsonData.length; i++) {
        var $divParent  = $("<div></div>");
        $divParent.text(jsonData[i].url).attr('id',jsonData[i].id).attr('style',"margin-left: 2em");
        console.log(jsonData[i].id);
        if (jsonData[i].children) {
            appendDom($divParent, jsonData[i].children);
        }
        container.append($divParent);
    }
  }

}

})