var form = document.querySelector(".form-login");

form.addEventListener("submit", postFunction);

function postFunction(e) {
    e.preventDefault();
    // console.log("post function");
    console.log(document.getElementById("lastname").value);
    console.log(document.getElementById("password").value);
    var sendInfo = {
        last_name: document.getElementById("lastname").value,
        first_name: document.getElementById("firstname").value,
        email: document.getElementById("email").value,
        password: document.getElementById("password").value,
        id_role: document.getElementById("roles").options[document.getElementById("roles").selectedIndex].value
    };
    // console.log(JSON.stringify(sendInfo));
    request = $.ajax({
        url: "../../api2/user/insert.php",
        type: "post",
        headers: {
            'Access-Control-Allow-Origin': '*',
          },
        dataType: "json",
        data: JSON.stringify(sendInfo),
        error: function (err) {
          console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
        },
        success: function(data) {
            alert(JSON.stringify(data)); // apple
            // console.log(document.getElementById("roles").options[document.getElementById("roles").selectedIndex.value]);
            location.replace("http://andrei-x550jx:81/frontend/login/index.html")
         }
      });
  }

  $(document).ready(function(){
    console.log("something");
    $.ajax({ 
      url: "../../api2/role/read.php",
      type: "GET",
        dataType: "json",
        error: function (err) {
          console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
        },
        success: function(data) {
            //alert(JSON.stringify(data)); 

            $.each(data,function(key,value){
              
              var o = new Option(value.role_name, value.id);
              /// jquerify the DOM object 'o' so we can use the html method
              //console.log(value.role_name);
              $(o).html(value.role_name);
              $("#roles").append(o);

            });
        }
    });
  });