<?php 
    class entity{

        public $conn;
        public $table_name = " ";

        public function __construct($conn, $table_name){
            $this->conn = $conn;
            
            $this->table_name = $table_name;
        }

        public function list(){
            
            $query = "SELECT * FROM " . $this->table_name;

            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;

        }

        public function add($atrib){
            
            $query = "INSERT INTO " . $this->table_name . " ( " ;// "VALUES(" . implode(",",$atrib) . ")";

            foreach($atrib as $key => $value) {
                if($key != "id")
                    $query = $query . $key . " , " ;
            }
            $query = substr($query, 0, -2);

            $query = $query . ") VALUES ( ";

            $values_atrib = array();

            foreach($atrib as $key => $value) {
                if($key != "id" && $key == "password"){
                    $query = $query . "? , " ;
                    array_push($values_atrib, password_hash($value, PASSWORD_DEFAULT));
                }else{
                    $query = $query . "? , " ;
                    array_push($values_atrib, $value);
                }
            }
            $query = substr($query, 0, -2);

            $query = $query . ")";

            //$query = "INSERT INTO users ( last_name , first_name , password , email , id_role ) VALUES ( 'q' , 'q' , 'q' , 'q' , 1 );";

            $stmt = $this->conn->prepare($query);
            
            $stmt->execute($values_atrib);
            //$stmt->execute();
            
            //echo json_encode(array("queryASASASA "=> $query));

        }

        public function edit($id, $atrib){
            $query = "UPDATE " . $this->table_name . " SET " ;

            foreach($atrib as $key => $value) {
                if($key != "id"){
                    if(gettype($value) == 'integer' || gettype($value) == 'double')
                        $query = $query . $key . " = " . $value . ", ";
                    else
                        $query = $query . $key . " = " . "'" . $value . "', ";
                
                }
                print_r();
            }

            $query = substr($query, 0, -2);

            $query = $query . " WHERE id = " . $id;

            

            $stmt = $this->conn->prepare($query);
            $stmt->execute();

        }

        public function delete($id){

            $query = "DELETE FROM " . $this->table_name . " WHERE id = " . $id;
            
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
        }

        public function listOne($id){
            $query = "SELECT * FROM " . $this->table_name . " WHERE id = " . $id;

            print_r($query);

            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }

    }

?>