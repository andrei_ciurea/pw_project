<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database_conn.php';
include_once '../objects/role.php';
 
// instantiate database and user object
$database = new database_conn();
$db = $database->getConnection();

session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    if (isset($_FILES['files'])) {
        $errors = [];
        $path = 'uploads/';
        if (!file_exists('uploads')) {
            echo json_encode(array("messajge" => mkdir('uploads', 0777, true)));
        }else{
            $path = $path . $_POST["subject_name"];
            $path = $path . '/';
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }else{
                $path = $path . $_POST["task_name"];
                $path = $path . '/';
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }else{
                    $path = $path .  $_SESSION["email"];
                    $path = $path . '/';
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                } 
            }
        }
    }
        
        
       // $path = 'uploads/' . $_POST["subject"] . '/' . $_POST["task_name"] . '/' . $_SESSION["email"] . '/';
        $extensions = ['jpg', 'jpeg', 'png', 'gif','zip','pdf'];

        $all_files = count($_FILES['files']['tmp_name']);

        for ($i = 0; $i < $all_files; $i++) {
            $file_name = $_FILES['files']['name'][$i];
            $file_tmp = $_FILES['files']['tmp_name'][$i];
            $file_type = $_FILES['files']['type'][$i];
            $file_size = $_FILES['files']['size'][$i];
            $tmp = explode('.', $_FILES['files']['name'][$i]);
            $file_ext = strtolower(end($tmp));

            $file = $path . $file_name;
            

            if (!in_array($file_ext, $extensions)) {
                $errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
            }

            if ($file_size > 2097152) {
                $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
            }

            if (empty($errors)) {
                
                $moved = move_uploaded_file($file_tmp, $file);
                

                $query_subject = "SELECT id FROM subjects WHERE subject_name = ?";

                if($stmt = $db->prepare($query_subject)){
                    
                    $params = array();
                    array_push($params,$_POST["subject_name"]);
                    $stmt->execute($params);
                    
                    if($stmt->rowCount() == 1){                    
                        // Bind result variables
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        extract($row);
                        

                        $subject_id = $id;
                        
                        $query_task = "SELECT id FROM tasks WHERE id_subject = ? and task_name = ?";

                        if($stmt = $db->prepare($query_task)){
                            
                            $params = array();
                            array_push($params,$subject_id);
                            array_push($params,$_POST["task_name"]);
                            $stmt->execute($params);
                            
                            if($stmt->rowCount() == 1){                    
                                // Bind result variables
                                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                                extract($row);
                                //echo json_encode(array("messages: " => ($id)));
                                $task_id = $id;

                                $query_submision = "INSERT INTO submission (id_user,id_task,submission_path,submission_date) VALUES ( ?, ?, ?, ?)";

                                if($stmt = $db->prepare($query_submision)){
                                    
                                    $params = array();
                                    array_push($params,$_SESSION["id"]);
                                    array_push($params,$task_id);
                                    array_push($params,$file);
                                    array_push($params,date("Y-m-d H:i:s"));
                                    
                                    echo json_encode($params);

                                    $stmt->execute($params);
                                    
                                }
                            }   
                        }
                    }
                }
            }
        }

        if ($errors) echo json_encode(array("messages: " => ($errors)));
        // else
        // { 
        //     if($moved){
        //         echo json_encode(array("message"=>"upload done"));
        //     }
        // }
    }
}

?>