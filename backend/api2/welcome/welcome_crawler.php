<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database_conn.php';
include_once '../objects/user.php';


$queue = array();
$checked = array();
$tree = array();
$parent_id = -1;
$elements = array();
$id = 0;
$queue["elements"] = array();
$urls_arr = array();

$element = array(
    "id" => $id,
    "parent_id" => $parent_id,
    "url" => "http://andrei-x550jx:81/frontend/login/index.html",
//    "children" => array()
);

array_push($urls_arr,$element['url']);
//echo json_encode($urls_arr);

array_push($queue["elements"], $element);


while(!empty($queue["elements"])){
    
    $element = array_shift($queue["elements"]);
    
    if(!in_array($element,$elements)){
        
        $html = file_get_contents($element['url']);

        $dom = new DOMDocument();

        //Parse the HTML. The @ is used to suppress any parsing errors
        //that will be thrown if the $html string isn't valid XHTML.
        $tr = @$dom->loadHTML($html);
        
        
        //Get all links. You could also use any other tag name here,
        //like 'img' or 'table', to extract other tags.
        // $table = $dom->querySelector('#taskSubject');
        $links = $dom->getElementsByTagName('a');
        
        //Iterate over the extracted links and display their URLs
        foreach ($links as $link){
            if($link->getAttribute('href') != "#")
            {
                if(!in_array($link->getAttribute('href'),$urls_arr) && $link->getAttribute('href') != ""){
                    $id += 1;
                    $chl = array(
                        "id" => $id,
                        "parent_id" => $element["id"],
                        "url" => $link->getAttribute('href'),
                        //"children" => array()
                    );
                // echo(json_encode(array("link" => $link->getAttribute('href'))));
                    //array_push($element["children"], $chl);
                
                    array_push($queue["elements"],$chl);
                    array_push($urls_arr,$link->getAttribute('href'));
                }
            }
        }


        // $table = $dom->getElementsByTagName('table')->item(0);
        
        // if(!is_null($table)){
                
        //     $lks = $table->getElementsByTagName('tr');
            
        //     echo json_encode(array("tb " => $dom->saveHTML($table)));
         
        //     //echo json_encode(array("ms" => $lks->item(0)->nodeValue));    
        //     foreach($lks as $lk){
        //         $trUrl = "";
        //         if(!in_array($trUrl,$urls_arr)){
                
        //             $trUrl = "Page = ";
        //             $tds = $lk->getElementsByTagName('td');
        //             $ct = 0;
                
        //             foreach($tds as $td){
        //                 if($ct < 2)
        //                     $trUrl += $td->nodeValue;
        //                 $ct += 1;
        //             }
        //             $id += 1;

        //             $chl = array(
        //                 "id" => $id,
        //                 "parent_id" => $element["id"],
        //                 "url" => $trUrl,
        //                 //"children" => array()
        //             );

        //             //array_push($element["children"], $chl);
        //             // echo json_encode(array("ms" => $trUrl));
        //             array_push($queue["elements"],$chl);
        //             array_push($urls_arr,$trUrl);
        //         }
        //     }
        // }
        
       // echo json_encode(array("message2 " =>  $element));


        $scripts = $dom->getElementsByTagName('script');
        //echo json_encode(array ("scripts" => $scripts));
        foreach($scripts as $script){
            
            if($script->getAttribute('src') != ""){
                if($script->getAttribute('src') != "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"){
                    
                    $url = build_url($element['url'],$script->getAttribute('src'));
                    $string = file_get_contents($url);
                    
                    $ln_ar = array();
                    $ln = readScriptsLocation($string, "location.replace(\"", "\")",$ln_ar);
                    //echo json_encode(array("ms"=>$script->getAttribute('src'), "element" => $element['url'],"mes ln" => $ln));
                    foreach($ln as $link){
                        if(!in_array($link,$urls_arr) && $link != ""){
                            $id += 1;
                            $chl = array(
                                "id" => $id,
                                "parent_id" => $element["id"],
                                "url" => $link,
                            // "children" => array()
                            );
                            
                            //echo json_encode(array("message1 " =>  $chl));
                            //array_push($element["children"], $chl);
                            
                            array_push($queue["elements"],$chl);
                            array_push($urls_arr,$link);
                        }
                    }
                }
            }else{
                $ln_ar = array();
                $ln = readScriptsLocation(readScriptsInHTML($dom->saveHTML($script),"<script>","</script>"),"location.replace(\"", "\")",$ln_ar);
                //echo json_encode($ln);
                foreach( $ln as $link){
                    //echo json_encode(array("message sa" => $link));
                    if(!in_array($link,$urls_arr) && $link != ""){
                        $id += 1;
                        $chl = array(
                            "id" => $id,
                            "parent_id" => $element["id"],
                            "url" => $link,
                            //"children" => array()
                        );
        
                        //array_push($element["children"], $chl);
                    
                        array_push($queue["elements"],$chl);
                        array_push($urls_arr,$link);
                    }
                     //echo json_encode(array("message2 " =>  $element, "message3" => $chl));
                     //echo json_encode(array("message2 " =>  $element));
                }
            }
        }
        array_push($elements, $element);
    }
}
//echo json_encode(array("message2 " =>  $urls_arr));
//echo json_encode($elements);
//buildTree($elements);
echo json_encode(buildTree($elements));

function readScriptsInHTML($string, $start, $end){
    
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    $next_lev = substr($string, $ini, $len);
    
    return $next_lev;
}


       


function readScriptsLocation($string, $start, $end, $links){

    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0 || $ini < 0 || $ini == false) {
        //echo json_encode($links);
        return $links;
    }
    
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    $next_url = substr($string, $ini, $len);
    // echo json_encode(array("message " => $next_url));

    array_push($links ,$next_url);
    
    return readScriptsLocation(strstr($string,$next_url),$start,$end, $links);
}

function build_url($url, $found_url){
    $string = '';
    $url_array = explode('/',$url);
    $found_url_copy = $found_url;
    $new_found_url_array = array();
    array_pop($url_array);
    foreach(explode('/',$found_url) as $fl){
        
        if($fl == ".."){
            array_pop($url_array);
        }else{
            if($fl != '.'){
                array_push($new_found_url_array,$fl);
            }
        }
    }
    foreach($url_array as $part){
        $string .= $part . "/";
    }
    foreach($new_found_url_array as $part){
        $string .= $part . "/";
    }
    $final_string = rtrim($string,"/");
    // echo json_encode(array("message" => $final_string));
    return $final_string;
}

function buildTree(array &$elements, $parentId = -1) {
    $branch = array();
    //echo json_encode($elements);
    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            //$branch[$element['id']] = $element;
            array_push($branch,$element);
            unset($elements[$element['id']]);
        }
    }
    return $branch;
}

function DOMinnerHTML(DOMNode $element) 
{ 
    $innerHTML = ""; 
    $children  = $element->childNodes;

    foreach ($children as $child) 
    { 
        $innerHTML .= $element->ownerDocument->saveHTML($child);
    }

    return $innerHTML; 
} 

?>