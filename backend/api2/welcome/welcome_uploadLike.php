<?php


// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database_conn.php';

session_start();
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"]){

    $database = new database_conn();
    $db = $database->getConnection();

    $requestBody = file_get_contents('php://input');

    $decodedBody =  json_decode($requestBody,true);
    echo json_encode($decodedBody["email"]);
    if($_SESSION["email"] == $decodedBody["email"]){
        $queryInserLikes = "INSERT INTO likes (id_comm, id_user, l_d) values (?, ?, ?)";

        if($stmt = $db->prepare($queryInserLikes)){
        
            $params = array();

            array_push($params,$decodedBody["id_comm"]);
            array_push($params,$_SESSION["id"]);
            array_push($params, $decodedBody["l_d"]);

            $stmt->execute($params);
        }
    }

}else{
    echo json_encode(
        array("location" => "http://andrei-x550jx:81/frontend/login/index.html")
    );
}
