<?php


// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database_conn.php';

session_start();
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"]){

    // instantiate database and user object
    $database = new database_conn();
    $db = $database->getConnection();

    $requestBody = file_get_contents('php://input');

    $decodedBody =  json_decode($requestBody,true);


    //get the id_task
    $task_id = 1;


    $query_subject = "SELECT id FROM subjects WHERE subject_name = ?";

    if($stmt = $db->prepare($query_subject)){
        
        $params = array();
        array_push($params,$decodedBody["subject_name"]);
        $stmt->execute($params);
        
        if($stmt->rowCount() == 1){                    
            // Bind result variables
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            extract($row);


            $subject_id = $id;
            
            $query_task = "SELECT id FROM tasks WHERE id_subject = ? and task_name = ?";

            if($stmt = $db->prepare($query_task)){
                
                $params = array();
                array_push($params,$subject_id);
                array_push($params,$decodedBody["task_name"]);
                $stmt->execute($params);
                
                if($stmt->rowCount() == 1){                    
                    // Bind result variables
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    extract($row);
                    //echo json_encode(array("messages: " => ($id)));
                    $task_id = $id;
                }
            }
        }
    }
    //echo json_encode(array("message"=>$decodedBody["subject_name"]));

    $queryComm = "SELECT id, comm_text, to_char(comm_createtime, 'yyyy.mm.dd hh:mi:ss') as comm_createtime, id_task, id_user FROM comm WHERE id_task = ?";
    $queryLikes = "SELECT * FROM  likes WHERE id_comm = ? and l_d = ?";
    $queryUser = "SELECT email FROM users WHERE id = ?";


    $values_atrib = array();
    array_push( $values_atrib, $task_id);

    $stmt = $db->prepare($queryComm);
    $stmt->execute($values_atrib);
    $num = $stmt->rowCount();

    if($num>0){
    
        // products array
        $comms_arr=array();
        $comms_arr["records"]=array();
        // retrieve our table contents
        // fetch() is faster than fetchAll()
        // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            // extract row
            // this will make $row['name'] to
            // just $name only
            extract($row);
            //echo json_encode(array("id" => $id, "id_1" => $row["id"]));

            $values_atrib_lk = array();
            array_push( $values_atrib_lk, $id);
            array_push( $values_atrib_lk, 1);

            $stmt_lk = $db->prepare($queryLikes);
            $stmt_lk->execute($values_atrib_lk);
            $num_lk = $stmt_lk->rowCount();

            $values_atrib_lk = array();
            array_push( $values_atrib_lk, $id);
            array_push( $values_atrib_lk, 0);

            $stmt_lk = $db->prepare($queryLikes);
            $stmt_lk->execute($values_atrib_lk);
            $num_dl = $stmt_lk->rowCount();

            $values_atrib_u = array();
            array_push( $values_atrib_u, $id_user);

            $stmt_u = $db->prepare($queryUser);
            $stmt_u->execute($values_atrib_u);

            $rowU = $stmt_u->fetch(PDO::FETCH_ASSOC);
            extract($rowU);

            $comm_item=array(
                "id" => $id,
                "email" => $email,
                "comm_text" => $comm_text,
                "comm_createTime" => $comm_createtime,
                "id_task" => $id_task,
                "id_user" => $id_user,
                "nr_likes" => $num_lk,
                "nr_dislikes" => $num_dl
            );
    
            array_push($comms_arr["records"], $comm_item);
        }
    
        // set response code - 200 OK
        http_response_code(200);
    
        // show products data in json format
        echo json_encode($comms_arr["records"]);
    }
    else{
    
        // set response code - 404 Not found
        http_response_code(404);
    
        // tell the user no products found
        echo json_encode(
            array("message" => "No records founds.")
        );
    }
}else{
    echo json_encode(
        array("location" => "http://andrei-x550jx:81/frontend/login/index.html")
    );
}

?>