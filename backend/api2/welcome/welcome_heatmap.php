<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
     
    // include database and object files
    include_once '../config/database_conn.php';
    include_once '../objects/user.php';
    
    $database = new database_conn();
    $conn = $database->getConnection();
    
    session_start();
     
    //Check if the user is already logged in, if yes then redirect him to welcome page
    if(!isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === false){
        http_response_code(200);
        echo json_encode( array ("location" => "http://andrei-x550jx:81/frontend/login/index.html"));
        exit;
    }

    $requestBody = file_get_contents('php://input');

    $decodedBody =  json_decode($requestBody,true);

    $query = "INSERT INTO heatmap (clientx, clienty, page, counter) VALUES (?, ?, ?, ?)";

    $stmt = $conn->prepare($query);
    $params = array();

    
        foreach($decodedBody as $item){
            array_push($params,$item["clientX"]);
            array_push($params,$item["clientY"]);
            array_push($params,$item["page"]);
            array_push($params,$item["counter"]);

            $stmt->execute($params);
            $params = array();
    }
    http_response_code(200);
    echo json_encode(array("mesage" => "ok"));
?>