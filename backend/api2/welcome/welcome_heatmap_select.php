<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
     
    // include database and object files
    include_once '../config/database_conn.php';
    include_once '../objects/user.php';
    
    $database = new database_conn();
    $conn = $database->getConnection();
    
    session_start();
     
    //Check if the user is already logged in, if yes then redirect him to welcome page
    if(!isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === false){
        http_response_code(200);
        echo json_encode( array ("location" => "http://andrei-x550jx:81/frontend/login/index.html"));
        exit;
    }

    $requestBody = file_get_contents('php://input');

    $decodedBody =  json_decode($requestBody,true);

    $query = "SELECT clientx, clienty, counter FROM heatmap WHERE page = ?";

    $stmt = $conn->prepare($query);
    $params = array();

    array_push($params,$decodedBody["page_name"]);
     
    $stmt->execute($params);
    $num = $stmt->rowCount();
    //echo $num;
    // check if more than 0 record found
    if($num>0){
    
        // products array
        $hm_arr=array();
        $hm_arr["records"]=array();
        // retrieve our table contents
        // fetch() is faster than fetchAll()
        // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            // extract row
            // this will make $row['name'] to
            // just $name only
            extract($row);
    
            $hm_item=array(
                "x" => $clientx,
                "y" => $clienty,
                "count" => $counter
            );
    
            array_push($hm_arr["records"], $hm_item);
        }

        $query = "SELECT max(counter) FROM heatmap WHERE page = ?";

        $stmt = $conn->prepare($query);
        $params = array();

        array_push($params,$decodedBody["page_name"]);
        
        $stmt->execute($params);
        $num = $stmt->rowCount();
        if($num == 1){
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            extract($row);
            $hm_arr["max"] = $max; 
        }
        
        // set response code - 200 OK
        http_response_code(200);
    
        // show products data in json format
        echo json_encode($hm_arr);
    }
    else{
    
        // set response code - 404 Not found
        http_response_code(404);
    
        // tell the user no products found
        echo json_encode(
            array("message" => "No users found.")
        );
        
    }
?>