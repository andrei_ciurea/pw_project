<?php
session_start();

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database_conn.php';
 
// instantiate database and user object
$database = new database_conn();
$db = $database->getConnection();

$requestBody = file_get_contents('php://input');

$decodedBody =  json_decode($requestBody,true);

$query = "SELECT id FROM comm where comm_text = ?";

if($stmt = $db->prepare($query)){
    $params = array();
    array_push($params,$decodedBody["comm_text"]);
    //echo json_encode($decodedBody["comm_text"]);
    $stmt->execute($params);
    if($stmt->rowCount() > 0){            
        $cm_arr=array();
        $cm_arr["records"]=array();        
        // Bind result variables
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $cm_item=array(
                //"id" => $id,
                "id" => $id
            );
     
            array_push($cm_arr["records"], $cm_item);
        }

        http_response_code(200);
 
        // show products data in json format
        echo json_encode($cm_arr["records"]);
    }else{
        http_response_code(404);
 
    // tell the user no products found
        echo json_encode(
            array("message" => "No records foundx." . $_SESSION["id"])
        );
    }
}
?>