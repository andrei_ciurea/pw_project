<?php
session_start();

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database_conn.php';
 
// instantiate database and user object
$database = new database_conn();
$db = $database->getConnection();

$query = "SELECT  s.subject_name, s.subject_desc FROM  subjects s,subject_user su where su.id_user = ?";

if($stmt = $db->prepare($query)){
    $session_id_array = array();
    array_push($session_id_array,$_SESSION["id"]);
    $stmt->execute($session_id_array);
    if($stmt->rowCount() > 0){            
        $table_arr=array();
        $table_arr["records"]=array();        
        // Bind result variables
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $table_item=array(
                //"id" => $id,
                "subject_name" => $subject_name,
                "subject_desc" => $subject_desc
            );
     
            array_push($table_arr["records"], $table_item);
        }

        http_response_code(200);
 
        // show products data in json format
        echo json_encode($table_arr["records"]);
    }else{
        http_response_code(404);
 
    // tell the user no products found
        echo json_encode(
            array("message" => "No records foundzz." . $_SESSION["id"])
        );
    }
}
?>