<?php


// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database_conn.php';

session_start();
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"]){

    $database = new database_conn();
    $db = $database->getConnection();

    $requestBody = file_get_contents('php://input');

    $decodedBody =  json_decode($requestBody,true);

    $task_id = 1;


    $query_subject = "SELECT id FROM subjects WHERE subject_name = ?";

    if($stmt = $db->prepare($query_subject)){
        
        $params = array();
        array_push($params,$decodedBody["subject_name"]);
        $stmt->execute($params);
        
        if($stmt->rowCount() == 1){                    
            // Bind result variables
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            extract($row);


            $subject_id = $id;
            
            $query_task = "SELECT id FROM tasks WHERE id_subject = ? and task_name = ?";

            if($stmt = $db->prepare($query_task)){
                
                $params = array();
                array_push($params,$subject_id);
                array_push($params,$decodedBody["task_name"]);
                $stmt->execute($params);
                
                if($stmt->rowCount() == 1){                    
                    // Bind result variables
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    extract($row);
                    //echo json_encode(array("messages: " => ($id)));
                    $task_id = $id;
                }
            }
        }
    }

    if($_SESSION["loggedin"] === true){
        $queryInserLikes = "INSERT INTO comm (id_task, id_user, comm_text, comm_createtime) values (?, ?, ?, current_timestamp)";

        if($stmt = $db->prepare($queryInserLikes)){
        
            $params = array();
            array_push($params,$task_id);
            array_push($params,$_SESSION["id"]);
            array_push($params,$decodedBody["comm_text"]);
            //array_push($params,"current_timestamp");
            //echo json_encode($params);

            $stmt->execute($params);
        }
    }

}else{
    echo json_encode(
        array("location" => "http://andrei-x550jx:81/frontend/login/index.html")
    );
}
