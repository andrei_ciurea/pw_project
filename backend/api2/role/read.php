<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database_conn.php';
include_once '../objects/role.php';
 
// instantiate database and user object
$database = new database_conn();
$db = $database->getConnection();
 
// initialize object
$role = new Role($db,"roles", array());
 
// query products
$stmt = $role->list();
$num = $stmt->rowCount();
// check if more than 0 record found
if($num>0){
 
    // products array
    $roles_arr=array();
    $roles_arr["records"]=array();
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $role_item=array(
            "id" => $id,
            "role_name" => $role_name,
        );
 
        array_push($roles_arr["records"], $role_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show products data in json format
    echo json_encode($roles_arr["records"]);
}
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No roles found.")
    );
    
}
// no products found will be here
?>