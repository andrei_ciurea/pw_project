<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database_conn.php';
include_once '../objects/role.php';
 
// instantiate database and user object
$database = new database_conn();
$db = $database->getConnection();

$requestBody = file_get_contents('php://input');

//var_dump($requestBody);

$decodedBody = json_decode($requestBody,true);

$role = new Role($db,"roles",$decodedBody);

$stmt = $role->listOne($role->atrib['id']);

$num = $stmt->rowCount();

//check if more than 0 record found
if($num>0){
 
    // products array
    
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    // extract row
    // this will make $row['name'] to
    // just $name only
    extract($row);

    $role_item=array(
        "id" => $id,
        "role_name" => $role_name,
    );
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show products data in json format
    echo json_encode($role_item);
}
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No role found.")
    );
    
}
// no products found will be here
?>