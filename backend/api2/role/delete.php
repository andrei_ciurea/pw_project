<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database_conn.php';
include_once '../objects/role.php';
 
// instantiate database and user object
$database = new database_conn();
$db = $database->getConnection();

$requestBody = file_get_contents('php://input');

//var_dump($requestBody);

$decodedBody = json_decode($requestBody,true);

$role = new Role($db,"roles",$decodedBody);

$role->delete($role->atrib['id']);

?>