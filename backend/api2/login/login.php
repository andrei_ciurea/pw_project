<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database_conn.php';
include_once '../objects/user.php';

$database = new database_conn();
$conn = $database->getConnection();

$placeholder_user = new User($conn,"users", array());

session_start();
 
//Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    http_response_code(200);
    echo json_encode( array ("location" => "http://andrei-x550jx:81/frontend/welcome/index.html"));
    exit;
}

$username = $passw = "";
$username_err = $password_err = "";

$requestBody = file_get_contents('php://input');

$decodedBody =  json_decode($requestBody,true);


 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Check if username is empty
    if(empty(trim($decodedBody["email"]))){
        $email_err = "Please enter email.";
    } else{
        $em = trim($decodedBody["email"]);
    }
    
    // Check if password is empty
    if(empty(trim($decodedBody["password"]))){
        $password_err = "Please enter your password.";
    } else{
        $passw = trim($decodedBody["password"]);
    }
    
    // Validate credentials
    if(empty($email_err) && empty($password_err)){
        // Prepare a select statement
        $query = "SELECT u.id, u.email, u.password, r.role_name FROM users u, roles r  WHERE email = ? AND u.id_role = r.id";
        
        if($stmt = $conn->prepare($query)){

            // Set parameters
            $param_email = $em;
            $params = array();
            array_push($params,$param_email);
            $stmt->execute($params);
            
            // Check if username exists, if yes then verify password
            if($stmt->rowCount() == 1){                    
                // Bind result variables
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                extract($row);

                if(password_verify($passw, $password)){
                    // Password is correct, so start a new session
                    //session_destroy();
                    session_start();
                    
                    // Store data in session variables
                    $_SESSION["loggedin"] = true;
                    $_SESSION["id"] = $id;
                    $_SESSION["email"] = $email;    
                    $_SESSION["role_name"] = $role_name;                        
                    
                    // Redirect user to welcome page
                    //header("location: welcome.php");
                    
                    http_response_code(200);
                    // show products data in json format
                    echo json_encode( array ("location" => "http://andrei-x550jx:81/frontend/welcome/index.html", "role_name" => $_SESSION["role_name"]));
                    
                } else{
                    // Display an error message if password is not valid
                    $password_err = "The password you entered was not valid.";
                    http_response_code(400);
 
                    // tell the user no products found
                    echo json_encode(
                        array("message" => "The password you entered was not valid.")
                    );
                }
            
            } else{
                // Display an error message if username doesn't exist
                $email_err = "No account found with that username.";
                http_response_code(400);
 
                // tell the user no products found
                echo json_encode(
                    array("message" => "No account found with that username.")
                );
            }
        }
    }
}

?>