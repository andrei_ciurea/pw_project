<?php

session_start();
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true && $_SESSION["role_name"] == "admin"){
    http_response_code(200);
    echo json_encode( array ("location" => "http://andrei-x550jx:81/frontend/welcome/index.html"));
    exit;
}else{
    http_response_code(400);
    echo json_encode( array ("location" => "http://andrei-x550jx:81/frontend/login/index.html"));
}

?>