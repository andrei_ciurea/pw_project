<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database_conn.php';
include_once '../objects/user.php';
 
// instantiate database and user object
$database = new database_conn();
$db = $database->getConnection();

$requestBody = file_get_contents('php://input');

$decodedBody =  json_decode($requestBody,true);

$user = new User($db,"users",$decodedBody);

echo json_encode($decodedBody);

$user->add($user->atrib);

?>