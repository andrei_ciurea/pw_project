<?php
session_start();

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database_conn.php';
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"]){
    // instantiate database and user object
    $database = new database_conn();
    $db = $database->getConnection();

    $query = "select c.comm_text, (select count(*) from likes l , comm x where l.id_comm = c.id group by l.id_comm) total from comm c ORDER BY total DESC limit 5";

    if($stmt = $db->prepare($query)){
        
        $stmt->execute();
        if($stmt->rowCount() > 0){            
            $table_arr=array();
            $table_arr["records"]=array();        
            // Bind result variables
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);

                $table_item=array(
                    "email" => $email,
                    "total" => $total
                );
        
                array_push($table_arr["records"], $table_item);
            }

            http_response_code(200);
    
            // show products data in json format
            echo json_encode($table_arr["records"]);
        }else{
            http_response_code(404);
    
        // tell the user no products found
            echo json_encode(
                array("message" => "No records foundss.")
            );
        }
    }
}
?>